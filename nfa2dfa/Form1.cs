﻿using System;
using System.Drawing;
using System.Windows.Forms;
using yWorks.Controls.Input;
using yWorks.Geometry;
using yWorks.Graph;
using yWorks.Graph.Styles;
using yWorks.Graph.LabelModels;
using Utils;
using System.Collections.Generic;

namespace nfa2dfa {
    public partial class Form1:Form {
        bool insertInitial = false, insertFinal = false;
        MemoryImageNodeStyle finalNode_style = new MemoryImageNodeStyle {
            Image=Image.FromFile("c://final.png")
        };
        MemoryImageNodeStyle initialNode_style = new MemoryImageNodeStyle {
            Image=Image.FromFile("c:\\initial.png")
        };
        public Form1() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            //RegisterToolStripButtonCommands();
            //RegisterMenuItemCommands();
            EnableGraphMLIO();
            ConfigureInteraction();
            SetDefaultLabelParameters();
            SetDefaultStyles();
            PopulateGraph();
            EnableUndo();
            UpdateViewport();
        }
        private void EnableGraphMLIO() {
            graphControl1.FileOperationsEnabled=true;
            graphControl_ans.FileOperationsEnabled=true;
        }
        public IGraph Graph {
            get { return graphControl1.Graph; }
        }
        
        private void EnableUndo() {
            Graph.SetUndoEngineEnabled(true);
            graphControl_ans.Graph.SetUndoEngineEnabled(true);
        }
        private void ConfigureInteraction() {
            graphControl1.InputMode=new GraphEditorInputMode();
            graphControl_ans.InputMode=new GraphEditorInputMode();
        }
        private void SetDefaultLabelParameters() {
            Graph.NodeDefaults.Labels.LayoutParameter=InteriorLabelModel.Center;
            EdgeSegmentLabelModel edgeSegmentLabelModel = new EdgeSegmentLabelModel();
            edgeSegmentLabelModel.Distance=10;
            Graph.EdgeDefaults.Labels.LayoutParameter=edgeSegmentLabelModel.CreateParameterFromSource(0, 0.5, EdgeSides.RightOfEdge);
            IGraph Graph_ = graphControl_ans.Graph;
            Graph_.NodeDefaults.Labels.LayoutParameter=InteriorLabelModel.Center;
            EdgeSegmentLabelModel edgeSegmentLabelModel_ = new EdgeSegmentLabelModel();
            edgeSegmentLabelModel_.Distance=10;
            Graph_.EdgeDefaults.Labels.LayoutParameter=edgeSegmentLabelModel_.CreateParameterFromSource(0, 0.5, EdgeSides.RightOfEdge);
        }
        private void PopulateGraph() {
            INode node0 = Graph.CreateNode(new PointD(50, 50));
            INode node1 = Graph.CreateNode(new PointD(150, 50));
            INode node2 = Graph.CreateNode(new PointD(150, 150));
            INode node3 = Graph.CreateNode(new PointD(50, 150),initialNode_style);
            INode node4 = Graph.CreateNode(new PointD(250, 150),finalNode_style);
            IEdge edge = Graph.CreateEdge(node2, node1);
            IEdge edge0 = Graph.CreateEdge(node2, node0);
            IEdge edge1 = Graph.CreateEdge(node3, node2);
            IEdge edge2 = Graph.CreateEdge(node2, node4);
            Graph.AddLabel(edge, "0");
            Graph.AddLabel(edge0, "1");
            Graph.AddLabel(edge1, "1");
            Graph.AddLabel(edge2, "0");
            Graph.AddLabel(node0, "node0");
            Graph.AddLabel(node1, "node1");
            Graph.AddLabel(node2, "node2");
            Graph.AddLabel(node3, "node3");
            Graph.AddLabel(node4, "node4");
        }
        private void SetDefaultStyles() {
            INodeStyle defaultNodeStyle = new ShinyPlateNodeStyle { Brush=new SolidBrush(Color.FromArgb(255, 255, 140, 0)) };
            Graph.NodeDefaults.Style=defaultNodeStyle;
            graphControl_ans.Graph.NodeDefaults.Style=defaultNodeStyle;
            var defaultEdgeStyle = new PolylineEdgeStyle { Pen=Pens.Gray };
            defaultEdgeStyle.TargetArrow=Arrows.Default;
            Graph.EdgeDefaults.Style=defaultEdgeStyle;
            graphControl_ans.Graph.EdgeDefaults.Style=defaultEdgeStyle;
            ILabelStyle defaultLabelStyle = new DefaultLabelStyle { Font=new Font("Tahoma", 12), TextBrush=Brushes.DarkRed };
            Graph.EdgeDefaults.Labels.Style=Graph.NodeDefaults.Labels.Style=defaultLabelStyle;
            graphControl_ans.Graph.EdgeDefaults.Labels.Style=graphControl_ans.Graph.NodeDefaults.Labels.Style=defaultLabelStyle;
            Graph.NodeDefaults.Size=new SizeD(40, 40);
            graphControl_ans.Graph.NodeDefaults.Size=new SizeD(40, 40);
        }
        private void UpdateViewport() {
            graphControl1.FitGraphBounds();
            graphControl_ans.FitGraphBounds();
        }
        [STAThread]
     /*   static void Main() {
            System.Windows.Forms.Application.EnableVisualStyles();
            System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);
            System.Windows.Forms.Application.Run(new Form1());
        }*/
        private void show_res(object sender, EventArgs e) {
            richTextBox1.Text="";
            Machine m0 = new Machine(new List<Transition>(), new List<State>());
           // try {
                foreach(IEdge ed in graphControl1.Graph.Edges) {
                    INode sourceNode = ed.GetSourceNode();
                    INode targetNode = ed.GetTargetNode();
                    bool isinitial = false, isfinal = false;
                    string name = sourceNode.Labels[0].Text;
                    richTextBox1.Text+=name;
                    if(sourceNode.Style.Equals(initialNode_style)) {
                        richTextBox1.Text+="(initial)";
                        isinitial=true;
                    }
                    if(sourceNode.Style.Equals(finalNode_style)) {
                        richTextBox1.Text+="(final)";
                        isfinal=true;
                    }
                    int sourceNode_number = m0.AddSingleState(new State(name, isinitial, isfinal));
                    richTextBox1.Text+="\t---";
                    richTextBox1.Text+=ed.Labels[0].Text;
                    richTextBox1.Text+="--->\t";
                    isinitial=false;isfinal = false; name = targetNode.Labels[0].Text;
                    richTextBox1.Text+=name;
                    if(targetNode.Style.Equals(initialNode_style)) {
                        isinitial=true;
                        richTextBox1.Text+="(initial)";
                    }
                    if(targetNode.Style.Equals(finalNode_style)) {
                        isfinal=true;
                        richTextBox1.Text+="(final)";
                    }
                    int targetNode_number = m0.AddSingleState(new State(name, isinitial, isfinal));
                    m0.AddTransition_byNumber(sourceNode_number, targetNode_number, ed.Labels[0].Text);
                    richTextBox1.Text+="\n";
                }
                m0.UpdateTransitions();
                Machine m1 = MainClass.NFA2DFA(m0);
                richTextBox1.Text+=m1.ToString();
                resault_btn.Text+=".";
            Show_ans_convertedStates(m1);
           // } catch { richTextBox1.Text="please enter all labels!! :)"; }
        }

        private void Show_ans_convertedStates(Machine m1) {
            IGraph g = graphControl_ans.Graph;
            List<INode> nodes = new List<INode>();
            List<ConvertedState> states = new List<ConvertedState>();
            int x = 250, y = 0;
            Random rnd = new Random();
            INode n;
            foreach(ConvertedState cs in m1.GetStates_multiple()) {
                if(cs.isInitial) {
                    n = g.CreateNode(new PointD(x, y), initialNode_style);
                    states.Add(cs);
                } else if(cs.isFinal) {
                    n =  g.CreateNode(new PointD(x, y), finalNode_style);
                    states.Add(cs);
                } else {
                    n = g.CreateNode(new PointD(x, y));
                    states.Add(cs);
                }
                nodes.Add(n);
                graphControl_ans.Graph.AddLabel(n, cs.name);
                x+=rnd.Next(-100, 100);
                y+=70;// rnd.Next(30, 100);
            }
            foreach(Transition t in m1.Transitions) {
                int source_node=-1, target_node=-1;
                for(int i=0;i<states.Count;i++){
                    if(t.Cstart_state.Equals(states[i])) {
                        source_node=i;
                        if(target_node!=-1) break;
                    }
                    if(t.Cnext_state.Equals(states[i])) {
                        target_node=i;
                        if(source_node!=-1) break;
                    }
                }
                IEdge e = g.CreateEdge(nodes[source_node], nodes[target_node]);
                graphControl_ans.Graph.AddLabel(e, t.symbol);
            }
        }

        private void final_btn_Click(object sender, EventArgs e) {
            IGraph g = graphControl1.Graph;
            if(!insertFinal) {
                insertFinal=true;
                insertInitial=false;
                g.NodeDefaults.Size=new SizeD(51, 51);
                g.NodeDefaults.Style=finalNode_style;
                final_btn.BackColor=Color.Red;
                initial_btn.BackColor=Color.White;
            } else {
                insertFinal=false;
                SetDefaultStyles();
                final_btn.BackColor=Color.White;
            }
        }
        private void initial_btn_click(object sender, EventArgs e) {
            IGraph g = graphControl1.Graph;
            if(!insertInitial) {
                insertInitial=true;
                insertFinal=false;
                richTextBox1.Text=g.NodeDefaults.Labels.ToString();
                g.NodeDefaults.Size=new SizeD(50, 50);
                g.NodeDefaults.Style=initialNode_style;
                final_btn.BackColor=Color.White;
                initial_btn.BackColor=Color.Red;
            } else {
                insertInitial=false;
                SetDefaultStyles();
                initial_btn.BackColor=Color.White;
            }
        }

    }
}
