﻿namespace nfa2dfa
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.final_btn = new System.Windows.Forms.Button();
            this.initial_btn = new System.Windows.Forms.Button();
            this.resault_btn = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.graphControl1 = new yWorks.Controls.GraphControl();
            this.graphControl_ans = new yWorks.Controls.GraphControl();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            this.toolStripContainer1.BottomToolStripPanelVisible = false;
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.graphControl_ans);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.final_btn);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.initial_btn);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.resault_btn);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.richTextBox1);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.graphControl1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1183, 497);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.LeftToolStripPanelVisible = false;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.RightToolStripPanelVisible = false;
            this.toolStripContainer1.Size = new System.Drawing.Size(1183, 497);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            this.toolStripContainer1.TopToolStripPanelVisible = false;
            // 
            // final_btn
            // 
            this.final_btn.Location = new System.Drawing.Point(23, 63);
            this.final_btn.Name = "final_btn";
            this.final_btn.Size = new System.Drawing.Size(75, 23);
            this.final_btn.TabIndex = 4;
            this.final_btn.Text = "final";
            this.final_btn.UseVisualStyleBackColor = true;
            this.final_btn.Click += new System.EventHandler(this.final_btn_Click);
            // 
            // initial_btn
            // 
            this.initial_btn.Location = new System.Drawing.Point(23, 34);
            this.initial_btn.Name = "initial_btn";
            this.initial_btn.Size = new System.Drawing.Size(75, 23);
            this.initial_btn.TabIndex = 3;
            this.initial_btn.Text = "initial";
            this.initial_btn.UseVisualStyleBackColor = true;
            this.initial_btn.Click += new System.EventHandler(this.initial_btn_click);
            // 
            // resault_btn
            // 
            this.resault_btn.Location = new System.Drawing.Point(23, 5);
            this.resault_btn.Name = "resault_btn";
            this.resault_btn.Size = new System.Drawing.Size(179, 23);
            this.resault_btn.TabIndex = 2;
            this.resault_btn.Text = "show_resault";
            this.resault_btn.UseVisualStyleBackColor = true;
            this.resault_btn.Click += new System.EventHandler(this.show_res);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(208, 5);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(275, 96);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // graphControl1
            // 
            this.graphControl1.BackColor = System.Drawing.Color.White;
            this.graphControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.graphControl1.DoubleClickSize = new yWorks.Geometry.SizeD(4D, 4D);
            this.graphControl1.DoubleClickTime = System.TimeSpan.Parse("00:00:00.5000000");
            this.graphControl1.DragSize = new yWorks.Geometry.SizeD(4D, 4D);
            this.graphControl1.Location = new System.Drawing.Point(0, 0);
            this.graphControl1.Name = "graphControl1";
            this.graphControl1.Size = new System.Drawing.Size(491, 497);
            this.graphControl1.TabIndex = 0;
            // 
            // graphControl_ans
            // 
            this.graphControl_ans.BackColor = System.Drawing.Color.White;
            this.graphControl_ans.Dock = System.Windows.Forms.DockStyle.Right;
            this.graphControl_ans.DoubleClickSize = new yWorks.Geometry.SizeD(4D, 4D);
            this.graphControl_ans.DoubleClickTime = System.TimeSpan.Parse("00:00:00.5000000");
            this.graphControl_ans.DragSize = new yWorks.Geometry.SizeD(4D, 4D);
            this.graphControl_ans.Location = new System.Drawing.Point(503, 0);
            this.graphControl_ans.Name = "graphControl_ans";
            this.graphControl_ans.Size = new System.Drawing.Size(680, 497);
            this.graphControl_ans.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1183, 497);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private yWorks.Controls.GraphControl graphControl1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button final_btn;
        private System.Windows.Forms.Button initial_btn;
        private System.Windows.Forms.Button resault_btn;
        private yWorks.Controls.GraphControl graphControl_ans;
    }
}

