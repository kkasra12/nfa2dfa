﻿using System;
using System.Collections.Generic;

namespace test
{
    public class State
    {
        public readonly string name;
        public readonly bool isInitial = false, isFinal = false;
        public IList<Transition> transitions = new List<Transition>();
        public State(string name)   {this.name = name;}
        public State(string name,bool isinitial)
        {
            this.isInitial = isinitial;
            this.name = name;
        }
        public bool Connect(Transition trans)
        {
            foreach(Transition t in this.transitions)
                if (t.Equals(trans))
                    return false;
            transitions.Add(trans);
            return true;
        }
        public bool Connect(string symbol, State next_state)
        {
            Transition tmp = new Transition(this, next_state, symbol);
            return this.Connect(tmp);
        }
        public List<State> Neighbours(string symbol)
        {
            List<State> ans = new List<State>();
            foreach (var t in transitions)
                if (t.symbol == symbol)
                    ans.Add(t.next_state);
            return ans;
        }
        public List<State> Transition_function(string symbol)
        {
            List<State> tmp = new List<State>() { this };
            for (int i = 0; i < tmp.Count; i++)
                for (int j = 0; j < tmp[i].transitions.Count; j++)
                    if (tmp[i].transitions[j].isEpsilon)
                    {
                        bool doesContain = false;
                        foreach (State s in tmp)
                            if (s.Equals(tmp[i].transitions[j].next_state))
                            { doesContain = true; break; }
                        if (!doesContain)
                            tmp.Add(tmp[i].transitions[j].next_state);
                    }
            List<State> ans = new List<State>();
            foreach (State s in tmp)
                foreach (Transition t in s.transitions)
                    if (t.symbol == symbol)
                    {
                        bool doesContain = false;
                        foreach (State st in ans)
                            if (st.Equals(t.next_state))
                            { doesContain = true; break; }
                        if (!doesContain)
                            ans.Add(t.next_state);
                    }

            for (int i = 0; i < ans.Count; i++)
                for (int j = 0; j < ans[i].transitions.Count; j++)
                    if (ans[i].transitions[j].isEpsilon)
                    {
                        bool doesContain = false;
                        foreach (State s in ans)
                            if (s.Equals(ans[i].transitions[j].next_state))
                            { doesContain = true; break; }
                        if (!doesContain)
                            ans.Add(ans[i].transitions[j].next_state);
                    }
            return ans;

        }
        public override bool Equals(object obj) 
        {
            State tmp = (State)obj;
            return this.name == tmp.name;
        }

        public override int GetHashCode()
        {
            var hashCode = 1469243992;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(name);
            hashCode = hashCode * -1521134295 + isInitial.GetHashCode();
            hashCode = hashCode * -1521134295 + isFinal.GetHashCode();
            return hashCode;
        }
        public override string ToString()
        {
            return this.name;
        }
    }
    public class Transition
    {
        public readonly State start_state, next_state;
        public readonly ConvertedState Cstart_state, Cnext_state;
        public readonly bool isConvertedState;
        public readonly string symbol;
        public readonly bool isEpsilon=false;
        public Transition(State start_state, State next_state, string symbol)
        {
            isConvertedState = false;
            this.start_state = start_state;
            this.next_state = next_state;
            this.symbol = symbol;
            this.isEpsilon |= symbol == "";
            start_state.Connect(this);
        }
        public Transition(State start_state, State next_state, bool isEpsilon)
        {
            isConvertedState = false;
            if (!isEpsilon) throw new Exception("WTF???!!! :|");
            this.isEpsilon = isEpsilon;
            this.start_state = start_state;
            this.next_state = next_state;
            start_state.Connect(this);
        }
        public Transition(ConvertedState start_state, ConvertedState next_state, string symbol)
        {
            isConvertedState = true;
            this.Cstart_state = start_state;
            this.Cnext_state = next_state;
            this.symbol = symbol;
            this.isEpsilon |= symbol == "";
            start_state.Connect(this);
        }
        public Transition(ConvertedState start_state, ConvertedState next_state, bool isEpsilon)
        {
            isConvertedState = true;
            if (!isEpsilon) throw new Exception("WTF???!!! :|");
            this.isEpsilon = isEpsilon;
            this.Cstart_state = start_state;
            this.Cnext_state = next_state;
            start_state.Connect(this);
        }

        public override bool Equals(object obj)
        {
            var tmp = (Transition)obj;

            if (isConvertedState)
                return this.Cstart_state.name == tmp.Cstart_state.name &&
                   this.Cnext_state.name ==  tmp.Cnext_state.name &&
                   this.symbol == tmp.symbol;
            else
                return this.start_state.name == tmp.start_state.name &&
                   this.next_state.name == tmp.next_state.name &&
                   this.symbol == tmp.symbol;
        }

        public override int GetHashCode()
        {
            var hashCode = -901525168;
            hashCode = hashCode * -1521134295 + isConvertedState.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(symbol);
            hashCode = hashCode * -1521134295 + isEpsilon.GetHashCode();
            return hashCode;
        }
        public override string ToString()
        {
            return this.start_state.name + "--" + this.symbol + "-->" + this.next_state.name;
        }
    }
    class Machine
    {
        public List<Transition> Transitions = new List<Transition>();
        private List<State> States = new List<State>();
        private List<ConvertedState> CStates = new List<ConvertedState>();
        public List<string> Alphabet = new List<string>();
        public State Initial_state;
        public ConvertedState CInitial_state;
        public readonly bool singleStates; //states or convertedstates r used ???
        public Machine(List<Transition> transitions, List<State> states)
        {
            this.Transitions.AddRange(transitions);
            this.States.AddRange(states);
            foreach (State s in states)
                if (s.isInitial)
                    this.Initial_state = s;
            this.singleStates = true;
            GatherAlphabet();
        }
        public Machine(List<Transition> transitions,List<ConvertedState> states)
        {
            this.Transitions.AddRange(transitions);
            this.CStates.AddRange(states);
            foreach (ConvertedState s in states)
                if (s.isInitial)
                    this.CInitial_state = s;
            this.singleStates = false;
            GatherAlphabet();
        }
        public List<State> GetStates_single()
        {
            if (singleStates) return States;
            else throw new Exception("Error!!(100)");
        }
        public List<ConvertedState> GetStates_multiple()
        {
            if (!singleStates) return CStates;
            else throw new Exception("Error!!(101)");
        }
        public bool SetStates(List<State> states)
        {
            if (singleStates) this.States = states;
            else throw new Exception("Error!!(102)");
            UpdateTransitions();
            return true;
        }
        public bool SetStates(List<ConvertedState> states)
        {
            if (!singleStates) this.CStates = states;
            else throw new Exception("Error!!(103)");
            UpdateTransitions();
            return true;
        }
        public bool AddStates(List<State> states)
        {
            if (singleStates) this.States.AddRange(states);
            else throw new Exception("Error!!(104)");
            UpdateTransitions();
            return true;
        }
        public bool AddStates(List<ConvertedState> states)
        {
            if (singleStates) this.CStates.AddRange(states);
            else throw new Exception("Error!!(105)");
            UpdateTransitions();
            return true;
        }
        public List<string> GatherAlphabet()
        {
            foreach(Transition t in this.Transitions)
            {
                bool doesContain = false;
                foreach(string tmp in this.Alphabet)
                    if (tmp==t.symbol) {doesContain = true;break;}
                if (!doesContain)
                    this.Alphabet.Add(t.symbol);
            }
            return this.Alphabet;
        }
        public List<Transition> UpdateTransitions()
        {
            if (singleStates)
                foreach (State s in this.States)
                    foreach (Transition t in s.transitions)
                    {
                        bool doesContain = false;
                        foreach (Transition tmp in this.Transitions)
                            if (tmp.Equals(t)) { doesContain = true; break; }
                        if (!doesContain)
                            this.Transitions.Add(t);
                    }
            else
                foreach (ConvertedState s in this.CStates)
                    foreach (Transition t in s.transitions)
                    {
                        bool doesContain = false;
                        foreach (Transition tmp in this.Transitions)
                            if (tmp.Equals(t)) { doesContain = true; break; }
                        if (!doesContain)
                            this.Transitions.Add(t);
                    }
            return this.Transitions;
        }
        public override string ToString()
        {
            string ans = "";
            if (this.singleStates)
                foreach (Transition t in Transitions)
                    ans += t.start_state.name + "--" + t.symbol + "-->" + t.next_state.name + "\n";
            else
                foreach(Transition t in Transitions)
                    ans += t.Cstart_state.name + "--" + t.symbol + "-->" + t.Cnext_state.name + "\n";
            return ans;
        }
    }
    public class ConvertedState
    {
        public List<State> IncludedStates = new List<State>();
        public readonly bool isInitial = false, isFinal = false;
        public readonly string name;
        public IList<Transition> transitions = new List<Transition>();
        public ConvertedState(string name,List<State> states)
        {
            this.name = name;
            this.IncludedStates.AddRange(states);
        }
        public override bool Equals(object obj)
        {
            ConvertedState tmp = (ConvertedState)obj;
            bool ans = true;
            foreach(State s in this.IncludedStates)
                foreach(State st in tmp.IncludedStates)
                    if(!s.Equals(st)) { ans = false; break; }
            return ans;
        }

        public override int GetHashCode() {return 0;}

        public bool Connect(Transition trans)
        {
            for (int i = 0; i < transitions.Count; i++)
                if (transitions[i].Equals(trans))
                    return false;
            transitions.Add(trans);
            return true;
        }
        public bool Connect(string symbol, ConvertedState next_state)
        {
            Transition tmp = new Transition(this, next_state, symbol);
            return this.Connect(tmp);
        }
        public override string ToString() 
        {
            string ans = this.name + ":[";
            foreach (State s in this.IncludedStates)
                ans += s.name + ", ";
            return ans + "]\n";
        }
    }
    class MainClass
    {
        public static void Main(string[] args)
        {
            State q0 = new State("a0",true);
            State q1 = new State("a1");
            State q2 = new State("a2");
            State q3 = new State("a3");
            State q4 = new State("a4");
            State q5 = new State("a5");
            State q6 = new State("a6");
            Transition t0 = new Transition(q0, q1, true);
            Transition t1 = new Transition(q1, q2, "0");
            Transition t2 = new Transition(q2, q2, "1");
            Transition t3 = new Transition(q0, q2, "1");
            Transition t4 = new Transition(q0, q3, true);
            Transition t5 = new Transition(q1, q5, true);
            Transition t6 = new Transition(q3, q5, true);
            Transition t7 = new Transition(q5, q6, "1");
            /*foreach (State i in q0.Transition_function("1"))
                Console.WriteLine(i.name);
            
            foreach (string symbol in nfa.GatherAlphabet())
                 Console.WriteLine(symbol);
                 */
            Machine nfa = new Machine(
           new List<Transition>() { t0, t1, t2, t3, t4, t5, t6, t7 },
           new List<State>() { q0, q1, q2, q3, q4, q5, q6 });
            Console.Write(NFA2DFA(nfa));


           


        }
        public static Machine NFA2DFA(Machine nfa)
        {
            List<Transition> transitions = new List<Transition>();
            List<State> tmp = new List<State>() { nfa.Initial_state };
            ConvertedState dead_state = new ConvertedState("deadState", new List<State>() { });
            ConvertedState initial_state = new ConvertedState("q0", tmp);
            List<ConvertedState> states = new List<ConvertedState>() { initial_state };
            Queue<ConvertedState> que = new Queue<ConvertedState>();
            que.Enqueue(initial_state);
            nfa.GatherAlphabet();
            int i = 1;
            while(12 == 10 + 2)
            {
                if (que.Count == 0) break;
                ConvertedState current_state = que.Dequeue();
                foreach(string symbol in nfa.Alphabet)
                {
                    if (symbol == null) continue;
                    ConvertedState next_converted_state = new ConvertedState("q"+i.ToString(),new List<State>());
                    foreach (State s in current_state.IncludedStates)
                        foreach(State destination_state in s.Transition_function(symbol))
                        {
                            bool does_include = false;
                            foreach(State s_tmp in next_converted_state.IncludedStates)
                                if(s_tmp.Equals(destination_state)) {does_include = true;break;}
                            if (!does_include)
                                next_converted_state.IncludedStates.Add(destination_state);
                        }
                    //now we should check if next_converted_state is new state or not
                    if (next_converted_state.IncludedStates.Count == 0)
                    {
                        Transition trans_tmp = new Transition(current_state, dead_state, symbol);
                        transitions.Add(trans_tmp);
                        current_state.Connect(trans_tmp);
                        continue;
                    }
                    bool is_new = false;
                    foreach(ConvertedState state_tmp in states)
                        if(state_tmp.Equals(next_converted_state)) {is_new = true;next_converted_state = state_tmp;break; }

                    Transition t = new Transition(current_state, next_converted_state, symbol);
                    current_state.Connect(t);
                    transitions.Add(t);
                    if (!is_new)
                    {
                        states.Add(next_converted_state);
                        que.Enqueue(next_converted_state);
                        i++;
                    }
                }
                
            }
            Machine dfa = new Machine(transitions, states);
            return dfa;
        }
    }
}
